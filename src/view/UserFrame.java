package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import control.Controller;
import model.BankAccount;

/**
   A frame that shows the growth of an investment with variable interest.
*/
public class UserFrame extends JFrame
{    
	private static final int FRAME_WIDTH = 450;
	private static final int FRAME_HEIGHT = 100;

	private static final double DEFAULT_RATE = 5;

     

   private JLabel rateLabel;
   private JTextField rateField;
   private JButton button;
   private JLabel resultLabel;
   private JPanel panel;
   
   private double result;
   
   

   public static void main(String[] args){
	   new Controller();
   }
   
   public UserFrame()
   {  
	  
      // Use instance variables for components 
      resultLabel = new JLabel("balance: " + result);

      // Use helper methods 
      createTextField();
      createButton();
      createPanel();
      
      setVisible(true);
      setSize(FRAME_WIDTH, FRAME_HEIGHT);
   }

   private void createTextField()
   {
      rateLabel = new JLabel("Interest Rate: ");

      final int FIELD_WIDTH = 10;
      rateField = new JTextField(FIELD_WIDTH);
      rateField.setText("" + DEFAULT_RATE);
   }
   
   public double getRate(){
	   double rate = Double.parseDouble(rateField.getText());
	return rate;
   }
   
   public void setAnswer(double result){
	   resultLabel.setText("balance: "+ result);
   }
  
   private void createButton()
   {
      button = new JButton("Add Interest");
   }
   public JButton getButton(){
	   return button;
    }
      
   private void createPanel()
   {
      panel = new JPanel();
      panel.add(rateLabel);
      panel.add(rateField);
      panel.add(button);
      panel.add(resultLabel);      
      add(panel);
   } 
}