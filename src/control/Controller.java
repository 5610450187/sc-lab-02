// ประกาศเป็น global variable เพราะใน class controller มีการใช้ตัวแปรของ model และ view ในการทำงานทั้งหมดไม่ได้ใช้ใน method ใด methodหนึ่ง

package control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import view.UserFrame;
import model.BankAccount;

public class Controller {
	
	private static final double INITIAL_BALANCE = 1000; 

	private BankAccount account;
	private UserFrame view;
	
	public static void main(String[] args) {
		new Controller();
	}
	
	public Controller(){
		account = new BankAccount(INITIAL_BALANCE);
		view = new UserFrame();
		
		ActionListener listener = new AddInterestListener();
		view.getButton().addActionListener(listener);
	}
	 class AddInterestListener implements ActionListener
     {
        public void actionPerformed(ActionEvent event)
        {
        	double rate = view.getRate();
        	double interest = account.getBalance() * rate / 100;
            account.deposit(interest);
            view.setAnswer(account.getBalance());
        }
     }


}
